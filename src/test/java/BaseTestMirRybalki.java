import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 27.04.18
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
public class BaseTestMirRybalki {
    static WebDriver driver = initChromedriver();
    @BeforeClass
    public static void BeforeClass() {
        driver.get("https://mir-rybalki.com/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

}
    @Test
    public void Test1(){
        WebElement setki = driver.findElement(By.xpath("/html/body/div[5]/div[1]/div[2]/ul/li[1]/div/ul/li[5]/a"));
        setki.click();
        WebElement rakolovki = driver.findElement(By.linkText("Ятеря,раколовки"));
        rakolovki.click();
        WebElement razmer = driver.findElement(By.id("link_to_product_689382518"));
        razmer.click();
        WebElement price = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/p/span[1]"));
        String priceText = price.getText();
        assertEquals("204,80", priceText);
        System.out.println("204,80");
        WebElement kupit = driver.findElement(By.linkText("Купить"));
        kupit.click();
        WebElement sravnit = driver.findElement(By.className("x-shc-total__price"));
        WebElement udalit = driver.findElement(By.className("x-shc-item__control-icon"));
        String sravnitText = sravnit.getText();
        assertEquals("204,80 грн.", sravnitText);
        System.out.println("204,80 - покупай!");
        udalit.click();
        System.out.println("передумал!");
        WebElement nachat = driver.findElement(By.className("x-empty-results__link"));
        nachat.click();


    }
    @Test
    public void Test2() {

        WebElement palatki = driver.findElement(By.xpath("/html/body/div[5]/div[1]/div[2]/ul/li[1]/div/ul/li[20]/a"));
        palatki.click();
        WebElement palatka = driver.findElement(By.id("link_to_product_12891194"));
        palatka.click();
        WebElement price = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/p/span[1]"));
        String priceText = price.getText();
        assertEquals("649,30", priceText);
        System.out.println("649,30 - normal!");
        WebElement kupil = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/a"));
        kupil.click();
        WebElement srprice = driver.findElement(By.className("x-shc-item__summary-price"));
        String srpriceText = srprice.getText();
        assertEquals("649,30 грн.", srpriceText);
        System.out.println("Цена в корзине совпадает!");
        driver.quit();




    }

    public static WebDriver initChromedriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }
}